/*********************************************************************

  cacol89. Algorithm Data Base

  Range tree implementation with lazy propagation

*********************************************************************/

/**
 * Range tree with lazy propagation.
 * This code implements in an array a range tree with
 * lazy propagation. The array implementation has
 * the tradeoff that the number of leaves always
 * has to be a power of 2, therefore it may waste
 * more space than necessary.
 *
 * ATENTION: make sure to call the procedure "rt_calc_num_leaves"
 * for initializing the structure, else it will not work properly!
 *
 * NOTE: there is something that can be confusing. The first leave
 * in the tree has index "rt_num_leaves". But when making queries
 * or updating the ranges are treated 0-indexed, where 0 is the first
 * leave (or first element in the array over which the tree is built).
 * Also note that the ranges are always treated with open end: [i-j)
 *
 * @param MAXN      The maximum amount of leaves. Careful
 *                  that this number has to actually be >=
 *                  to the closest upper power of 2 to the
 *                  actual number.
 *
 * @time: build O(n), query&update O(logn)
 * @test: (spoj)HORRIBLE, LITE
 */

//This number has to be >= to the smallest power of 2 >= to
//the actual maximum number of leaves in the range tree.
//For example if the tree has maximum 100 leaves, then MAXN = 128.
#define MAXN 131080

#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a<b?b:a)

typedef long long lint;

lint range_tree[2*MAXN+1], rt_prop[MAXN+1];
//This integer will hold the number of leaves in the tree,
//it is also the index in the array range_tree where the
//first leave is. I.E. the elements range_tree[rt_num_leaves],
//through range_tree[2*rt_num_leaves-1] inclusive are all the
//leaves in the range tree.
int rt_num_leaves=0;

//MAKE SURE TO CALL THIS PROCEDURE WHEN INITIALIZING
//THE STRUCTURE.
//This will calculate how many leaves the range tree has. The
inline lint rt_calc_num_leaves(int n){
    int msk=1;
    while(msk<n) msk<<=1;
    rt_num_leaves = msk;
}

inline bool rt_is_leave(int node){
    return node>=rt_num_leaves;
}

inline void rt_propagate(int node, int l, int r){
    lint c = rt_prop[node];
    if(c==0) return;
    int ln = 2*node, rn = 2*node+1;
    lint n = (r-l)/2;
    range_tree[ln] += c*n;
    range_tree[rn] += c*n;
    if(!rt_is_leave(ln)){
        rt_prop[ln] += c;
        rt_prop[rn] += c;
    }
    rt_prop[node] = 0;
}

//Before building, make sure to fill by hand the elements
//in the tree's leaves (from range_tree[rt_num_leaves] to
//range_tree[2*rt_num_leaves-1] inclusive)
void rt_build(int node=1, int l=0, int r=rt_num_leaves){
    if(l+1==r) return;

    int ln = 2*node,
        rn = 2*node+1,
        m = (l+r)/2;

    rt_build(ln,l,m);
    rt_build(rn,m,r);
    range_tree[node] = range_tree[ln]+range_tree[rn];
    rt_prop[node] = 0;
}

//This queries the segment [i-j), the elements are 0-indexed
lint rt_query(int i, int j, int node=1, int l=0, int r=rt_num_leaves){
    if(i>=r || j<=l) return 0;
    if(l==i && r==j) return range_tree[node];

    rt_propagate(node,l,r);

    int ln = 2*node,
        rn = 2*node+1, 
        m = (l+r)/2;
    lint lq = rt_query(i,MIN(j,m),ln,l,m),
         rq = rt_query(MAX(i,m),j,rn,m,r);

    return lq+rq;
}

//This queries the segment [i-j), the elements are 0-indexed
void rt_update(int i, int j, lint c, int node=1, int l=0, int r=rt_num_leaves){
    if(i>=r || j<=l) return;
    
    range_tree[node] += c*((lint)(j-i));
    
    if(l==i && r==j) {
        if(!rt_is_leave(node)) rt_prop[node]+=c;
        return;
    }

    rt_propagate(node,l,r);

    int ln = 2*node,
        rn = 2*node+1, 
        m = (l+r)/2;
    rt_update(i,MIN(j,m),c,ln,l,m);
    rt_update(MAX(i,m),j,c,rn,m,r);
}
