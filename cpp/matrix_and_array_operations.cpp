/*********************************************************************

  cacol89. Algorithm Data Base

  Matrix and array operations

*********************************************************************/

/**
  This procedure takes a matrix (A) and turns it into
  eight (8) other matrices:
    - hor: The same matrix
    - ver: The transposed matrix
    - pdiag: The positive diagonals of the matrix
    - ndiag: The negative diagonals of the matrix
    - rhor, rver, rpdiag, rndiag: The same matrices but with the
        elements of each row inverted (E.G. pdiag is equal than rpdiag
        but in each row the first element is the last of rpdiag and so on)

    At the end of the execution, the array pdl will contain the lenght of
    each positive diagonal, the same with ndl.

    @param A    The matrix to transform
    @param m    The number of rows in A
    @param n    The number of columns in A

    @time O( m*n )
    @test No

*/

#define MAXM 100
#define MAXN 100
void transform_matrix( char A[MAXM][MAXN], int m, int n ){

    char hor[MAXM][MAXN];
    char ver[MAXN][MAXM];
    char ndiag[MAXM+MAXN-1][min(MAXM,MAXN)];
    char pdiag[MAXM+MAXN-1][min(MAXM,MAXN)];
    char rhor[MAXM][MAXN];
    char rver[MAXN][MAXM];
    char rndiag[MAXM+MAXN-1][min(MAXM,MAXN)];
    char rpdiag[MAXM+MAXN-1][min(MAXM,MAXN)];

    int pdl[MAXM+MAXN-1];
    int ndl[MAXM+MAXN-1];

    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            char c = A[i][j];

            hor[i][j]=c;
            ver[j][i]=c;

            int nd = i-j+n-1;
            int pd = i+j;
            int nde = i < j ? i : j;
            int pde = pd < n ? i : (n-1)-j;

            ndiag[nd][nde]=c;
            pdiag[pd][pde]=c;

            //Reverses
            rhor[i][(n-1)-j]=c;
            rver[j][(m-1)-i]=c;

            //calc ndl
            {
                int comp = (nd<n?n:m)-min(m,n);
                int abs_nd = nd-(n-1);
                if( abs_nd < 0 ) abs_nd *= -1;
                ndl[nd] = min(m,n) - (abs_nd<=comp?0:abs_nd-comp);
            }
            //calc pdl
            {
                int l;
                if(pd < min(m,n))
                    l = pd+1;
                else if(pd+1 > max(m,n))
                    l = min(m,n) - (pd+1-max(m,n));
                else
                    l = min(m,n);
                pdl[pd] = l;
            }

            rndiag[nd][(ndl[nd]-1)-nde]=c;
            rpdiag[pd][(pdl[pd]-1)-pde]=c;
        }
    }
}

/**
  Selection algorithm.
  Given an array of elements, this algorithm finds the ith
  smallest element in the array. It uses the quicksort
  partition algorithm choosing a random pivot.
  Iterative implemenation.
    Note: The ith smallest element of the array is the element
    that, takes the position i-1 when the array is sorted

  @param a      The array where to search
  @param n      The size of the array
  @param ith    The number of the element to select (ith element)
  @returns      The ith smallest element in the array

  @include ctime cstdlib
  @time O(n) (average, it could be O(n²) in worst case, but
             the randomization makes it very unlikely to hapen,
             the probability will be 1/n! of having n² operations)
  @test uva(10041) 
*/

inline void swap(int *a, int i, int j){
    int aux = a[i];
    a[i] = a[j];
    a[j] = aux;
}

inline int quickselect(int *a, int n, int ith){

    srand((unsigned)time(NULL));

    int lb = 0, hb = n;

    while(true){
        //Select random pivot
        int range = hb-lb;
        int pivot = rand()%range + lb;
        int pval = a[pivot];

        swap(a,pivot,hb-1);

        int k = lb;
        for(int i=lb;i<hb-1;i++){
            if(a[i] < pval){
                swap(a,i,k++);
            }
        }
        swap(a,k,hb-1);

        if( k == ith-1 )
            return pval;

        //tail recursion
        if( k < ith-1 ){
            lb = k+1;
        } else { //k > ith-1
            hb = k;
        }
    }
}
