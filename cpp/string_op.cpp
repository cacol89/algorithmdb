/*********************************************************************

  cacol89. Algorithm Data Base

  String Operations Algorithm

*********************************************************************/

/**
    Knuth-Morris-Pratt algorithm
    Given a text T and a pattern P, the algorithm
    finds the first ocurrence of P in T.
    This can be easily modified to return either all the occurrences
    of P in T or to start with a given offset.

    @param T    The text to search, as a string.
    @param P    The pattern to find in T. As a string.
    @return     -1 If the pattern is not found in T, else, an integer
                i such that the pattern P occurs in T with offset i.
                E.G. T[i]T[i+1]...T[i+|P|-1] is the pattern.

    @include    cstring
    @time       O(|T|+|P|)
    @test       No
*/

#define KMP_MAXP  100 //Max size of |P|

void kmp_compute_prefix_function(char *P, int *phi);

int kmp_matcher(char *T, char *P){

    int prefix_fun[KMP_MAXP];
    kmp_compute_prefix_function(P,prefix_fun);

    int q = 0; //# of characters matched 
     
    for(int i=0; T[i]!='\0' ;i++){

        while( q>0 && P[q]!=T[i] ){
            q = prefix_fun[q];
        }
        if( P[q] == T[i] ){ //Next character matches
            q++;
        }
        if( P[q] == '\0' ){ //The pattern was matched
            return i - q + 1;
            //If want to search for more occurrences of P:
            //q = prefix_fun[q];
            //q = 0; //<- Non-overlapping ocurrences
        }   
    }   
    return -1; //no match found
}

void kmp_compute_prefix_function(char* P, int *phi){

    phi[0] = 0; 

    int k = 0; 
    for(int q=1; P[q]!='\0'; q++){

        phi[q] = k;

        while( k>0 && P[k]!=P[q] )
            k = phi[k-1];

        if( P[k] == P[q] )
            k++;

    }   
}
//phi[i] = j means that if you were trying to
//match the character in P[i] and it failed, then
//you should try to match the character in P[j]
//(j < i) because the string P[0..j-1] is a right
//substring of P[0..i-1]
