/********************************************************************
  cacol89. Algorithm Data Base

  Number Theory
********************************************************************/


/*
   GCD - Euclidean algorithm (iterative).

   @param a     A positive integer
   @param b     A positive integer
   @return      The GCD between a and b

   @time        (worst case) O( log(min{a,b}) )
   @tested      No
*/
inline int gcd(int a, int b){
    while( b!=0 ){
        int aux = b;
        b = a%b;
        a = aux;
    }
    return a;
}

/*
   LCM - Using the Euclidean algorithm

   @param a     A positive integer
   @param b     A positive integer
   @return      The LCM between a and b

   @time        (worst case) O( log(min{a,b}) )
   @tested      No
*/
inline int lcm(int a,int b){
    return (a*b)/gcd(a,b);
}

/*
    Extended GCD - Etended Euclidean algorithm.
    Besides returning the gcd of two numbers, this
    algorithm stores in 'x' and 'y' values that satisfy the
    Bezouts Identity: "ax + by = gcd(a,b)".

    Note: If a and b are coprime, then 'x' will hold the
    aritmetic inverse of 'a' in the 'mod b' ring of integers
    { a*x == 1 (mod b) }. Remember that 'x' could be negative.

    @param a     A positive integer
    @param b     A positive integer
    @param x     Out parameter
    @param y     Out parameter
    @return      The GCD between a and b

    @time        (worst case) O( log(min{a,b}) )
    @tested      Yes (check chineese reminder theorem)
*/
inline int egcd(int a, int b, int &x, int &y){
  if(b==0){
    x = 1; y = 0;
    return a;
  }
  int d = egcd(b,a%b,y,x);
  y -= x*(a/b);
  return d;
}
/*
    Modular Multiplicative Inverse (iterative)

    Given coprime positive integers 'a' and 'm' (gcd(a,m)==1),
    this algorithm calculates a⁻¹, the modular multiplicative
    inverse of 'a'. This is the minimum number 'x' that satisfies
    the equation: { a*x == 1 (mod m) }

    NOTE: If negative numbers are passed, the function will
    return nonsense results.

    @param a     A positive integer
    @param m     A positive integer
    @return      a⁻¹ if gcd(a,m)==1, else 0

    @time        (worst case) O( log(a) )
    @tested      hartals(uva 10050)
*/
inline int mod_mult_inverse(int a, int m){

    int x = 0, lastx = 1;
    int y = 1, lasty = 0;

    int b = m;
    int aux;
    while( b!=0 ){
        int quotient = a/b;
        int aux;
        //{a, b} = {b, a bod b}
        aux = b;
        b = a%b;
        a = aux;
        //{x, lastx} = {lastx - quotient*x, x}
        aux = x;
        x = lastx - quotient*x;
        lastx = aux;
        //{y, lasty} = {lasty - quotient*y, y}        
        aux = y;
        y = lasty - quotient*y;
        lasty = aux;
    }
    if(a!=1)
        return 0;
    x = lastx;
    if( x < 0 )
        return x + m;
    return x;

}
/*
    Chinese reminder theorem

    Finds all numbers "x" that solve the equations:
    
    x == a0 (mod n0)
    x == a1 (mod n1)

    The returned solution are numbers "a" and "n" such
    that a+kn solve the above equation for any value
    of k. These numbers are returned in "a0" and "n0"
    respectively. The number "a" is the smallest positive
    integer that satisfies the equation.

    If the system has no solution, false will be returned.
    True otherwise.
    NOTE: this algorithm will fail and may generate an
    error if it receives negative integers.
    
    @time        (worst case) O( log(min{n0,n1}) ),
                 (expected) O(1)
    @test        (spoj)SHUFFLE1,(codeforces)348B
*/
int egcd(int a, int b, int &x, int &y){
  if(b==0){
    x = 1; y = 0;
    return a;
  }
  int d = egcd(b,a%b,y,x);
  y -= x*(a/b);
  return d;
}
bool crt(int &a0,int &n0,int a1,int n1){
    int x,y;
    int d = egcd(n0,n1,x,y);
    if(a0%d != a1%d) return false;
    a0 = (a1/d)*n0*x + (a0/d)*n1*y + a0%d;
    n0 = n0/d*n1;
    if(a0 < 0) a0 = n0-((-a0)%n0);
    a0 %= n0;
    return true;
}
