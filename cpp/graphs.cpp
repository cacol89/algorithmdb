/*********************************************************************

  cacol89. Algorithm Data Base

  Graph Algorithms

*********************************************************************/

/**
 * Implicit Breadth-First Search.
 * This algorithm makes a BFS on an implicit graph, note that
 * the class node has some methods that are needed for creating
 * the implicit graph
 *
 * @param init      The initial node
 * @param target    The target node
 *
 * @uses: vector, queue, utility
 * @time: O(E+V)
 * @test: (uva)10067
 */
#define IBFS_MAXV 1000

class node{
  public:
    node(){}
    bool operator==(const node &co) const{
        return true;
    }
    vector<node> get_sons(){
        vector<node> v;
        return v;
    }
    int to_int(){
        return 0;        
    }
};

int implicit_bfs(node &init, node &target){

    bool visit[IBFS_MAXV];
    memset(visit,false, sizeof visit);

    queue< pair<node,int> > q;
    q.push( make_pair(init,0) );
    visit[ init.to_int() ] = true;

    pair<node,int> x;

    while(!q.empty()){
        x = q.front();
        q.pop();

        if(x.first==target){
            break;
        }

        vector<node> sons = x.first.get_sons();

        for(int i=0;i<sons.size();i++){
            if(!visit[sons[i].to_int()]){
                visit[sons[i].to_int()] = true;
                int ndist = x.second+1;
                q.push(make_pair(sons[i],ndist));
            }
        }

    }

    if(x.first==target){
        return x.second;
    }
    else {
        return -1;
    }
}

/**
 * Kuhn's algorithm.
 * Finds the maximum matching for a bipartite graph.
 *
 * @param MAXN          The maximum number of nodes in the LEFT OR RIGHT part of the
 *                      bipartite graph
 * @param u             Number of nodes in the LEFT part of the graph.
 * @param v             Number of nodes in the RIGHT part of the graph.
 * @param graph         Edges from the LEFT part of the graph to the RIGHT part,
 *                      where nodes on either side are numbered starting from 0,
 *                      (NOTE notes on the RIGHT too! from 0 to v-1).
 * @return  m1          m1[i] indicades whether node i on the LEFT was matched
 *                      or not
 * @return  m2          If m2[j] == -1, then node j on the RIGHT was not matched, 
 *                      else node j is matched with node m2[j], i.e. edge (m2[j],j)
 *
 * @time: O(V*E)
 * @test: (live archive) 6525
 */

//..........Params begin.........
#define MAXN 10000
vector< vector<int> > graph;
int u,v;
//..........Params end.........

bool m1[MAXN],visit[MAXN];
int m2[MAXN];

bool dfs(int x){
    visit[x]=true;
    for(int i=0;i<graph[x].size();i++){
        int y = graph[x][i];
        if(  m2[y]==-1 || (!visit[m2[y]] && dfs(m2[y])) ){
            m2[y]=x, m1[x]=true;
            return true;
        }
    }
    return false;
}

inline int kuhn(){
    memset(m1,false,sizeof(bool)*u);    
    memset(m2,-1,sizeof(int)*v);
    int tot=0;
    for(int i=0;i<u;i++){
        if(m1[i]) continue;
        memset(visit,false,sizeof(bool)*u);    
        if(dfs(i)) tot++;
    }
    return tot;
}

/**
 * Hopcroft-Karp algorithm.
 * Finds the maximum matching for a bipartite graph.
 *
 * @param MAXN          The maximum number of nodes in the whole graph + 1
 * @param MAXU          The maximum number of nodes in the LEFT part of the
 *                      bipartite graph + 1
 * @param INF           > MAXN
 * @param graph         Edges from the LEFT part of the graph to the RIGHT part,
 *                      the node 0 IS RESERVED (start by node 1).
 * @param u             Number of nodes in the LEFT part of the graph.
 * @param v             Number of nodes in the RIGHT part of the graph.
 *
 * @return  match       If match[i] == 0, then node i is not matched, else the
 *                      edge (i,match[i]) is matched. Node 0 is reserved, this
 *                      array covers both nodes on the LEFT and RIGHT parts.
 * @time: O(E*sqrt(V))
 * @test: (spoj)matching,quest4 (uva)663,10080
 */

//..........Params begin.........
#define MAXN 60
#define MAXU 30
#define INF 1<<28

vector< vector<int> > graph;
int u,v;
//..........Params end.........

int match[MAXN+1];
int n, stk[MAXU+1], stk_top, dist[MAXU+1];

inline bool bfs(){
    stk_top = 0;
    queue<int> q;
    for(int i=1;i<=u;i++){
        if(match[i]==0) {
            stk[stk_top++] = i, dist[i] = 0;
            q.push(i);
        }
        else dist[i] = INF;
    }
    dist[0] = INF;
    while(!q.empty()){
        int x = q.front(); q.pop();
        if(dist[x] >= dist[0]) break;
        for(int i=0;i<graph[x].size();i++){
            int y = match[graph[x][i]];
            if(dist[y]!=INF) continue;
            dist[y] = dist[x]+1;
            if(y!=0) q.push(y);
        }
    }
    return dist[0] != INF;
}

bool dfs(int x){
    if(x==0) return true;
    for(int i=0;i<graph[x].size();i++){
        int k = graph[x][i], y = match[k]; // x -> k ~> y
        if(dist[x]+1 == dist[y] && dfs(y)) {
            match[x] = k, match[k] = x;
            dist[x] = INF;
            return true;
        }
    }
    return false;
}

inline int hopcroft_karp(){
    n = u+v;
    memset(match,0,sizeof(int)*(n+1));
    int tot = 0;
    while(bfs())
        while(stk_top)
            if(dfs(stk[--stk_top])) tot++;
    return tot;
}


/**
 * Tarjan's algorithm for finding biconnected componets in UNDIRECTED GRAPHS.
 * 
 * Finds all bridges (isthmus, cut-edges), articulation points (cut-vertex),
 * and biconnected components in a provided undirected graph.
 *
 * @param MAXN          The maximum number of nodes in the graph.
 * @param graph         Adjecency-list description of the graph.
 * @param n             Number of nodes in the graph.
 * @return bridges      All bridges in the graph.
 * @return b_comp       All biconnected components in the graph, the
 *                      components are given as a set of non-repeated
 *                      edges. The direction of the edges follow
 *                      a DFS in the biconneced component.
 * @return a_points     All articulation points in the provided graph.
 *
 * @time: O(|E|+|V|)
 * @test: (uva)796, 610, 315
 */

//........Params begin..................
#define MAXN 10000
vector< vector<int> > graph;
int n;
//........Params end..................

vector< vector< pair<int,int> > > b_comp;
vector< pair<int,int> > bridges;
vector< int > a_points;
stack< pair<int,int> > stk;
int depth[MAXN], low[MAXN];

inline void new_b_comp(int v, int w){
    int x = b_comp.size();
    b_comp.push_back(vector< pair<int,int> >());
    bool stop = false;
    while(!stop){
        b_comp[x].push_back(stk.top());
        stop = stk.top().first == v && stk.top().second == w;
        stk.pop();
    }
}

void b_comp_dfs(int p, int v){
    low[v] = depth[v];
    bool is_a_point = false;
    for(int i=0;i<graph[v].size();i++){
        int w = graph[v][i];
        if(w==p || depth[w] > depth[v]) continue;
        stk.push(make_pair(v,w));
        if(depth[w]==-1){
            depth[w] = depth[v]+1;
            b_comp_dfs(v,w);
            low[v] = min(low[v],low[w]);
            if(low[w] >= depth[v]) //v is an articulation point
                is_a_point = true, new_b_comp(v,w);
            if(low[w]==depth[w]) //v - w is a bridge
                bridges.push_back(make_pair(min(v,w),max(v,w)));
        }
        else low[v] = min(low[v],depth[w]);
    }
    //root-nodes need a diffetent analysis
    if(depth[v]!=0 && is_a_point) a_points.push_back(v);
}

//Main function. The for cycle can be erased if it is
//warrantied that the graph is connected.
inline void find_b_comp(){
    b_comp.clear(), bridges.clear(), a_points.clear();
    memset(depth,-1,sizeof(int)*n);
    stk = stack< pair<int,int> >();
    for(int i=0;i<n;i++) if(depth[i]==-1) {
        depth[i] = 0, b_comp_dfs(-1,i);
        //Analyze if i is an a_point
        int count = 0;
        for(int j=0;j<graph[i].size() && count<2;j++)
            if(depth[graph[i][j]]==1) count++;
        if(count > 1) a_points.push_back(i);
    }
}

/**
 * Tarjan's algorithm for finding strongly connected components in DIRECTED GRAPHS.
 *
 * Calculates a partition of the graph's nodes in strongly connected components.
 *
 * @param MAXN          The maximum number of nodes in the graph.
 * @param graph         Adjecency-list description of the graph.
 * @param n             Number of nodes in the graph.
 * @return sc_comp_cnt  Contains the number of different strongly connected
 *                      components found.
 * @return sc_comp      Contains the str.conn.comp. to which it vertex belongs to,
 *                      i.e. sc_comp[i] = #of the str.conn.comp. of vertex i.
 *
 * @time: O(|E|+|V|)
 * @test: (spoj)BOTTOM
 */

//........Params begin..................
#define MAXN 5010

vector< vector<int> > graph;
int n;
//........Params end..................

int sc_comp[MAXN], sc_comp_cnt; 
int visit[MAXN], low[MAXN];
bool active[MAXN];
stack<int> stk;

inline void new_sc_comp(int v){
    bool stop = false;
    while(!stop){
        sc_comp[stk.top()] = sc_comp_cnt;
        active[stk.top()] = false;
        stop = stk.top() == v;
        stk.pop();
    }
    sc_comp_cnt++;
}

void scc_dfs(int v, int &step){
    low[v] = visit[v];
    active[v] = true, stk.push(v);
    for(int i=0;i<graph[v].size();i++){
        int w = graph[v][i];
        if(visit[w] == -1){
            visit[w] = step++;
            scc_dfs(w,step);
            low[v] = min(low[v],low[w]);
        }
        else if(active[w]) low[v] = min(low[v],visit[w]);
    }
    if(low[v]==visit[v]) new_sc_comp(v);
}

inline void find_sc_comps(){
    memset(visit,-1,sizeof(int)*n);
    //memset(active,false,sizeof(bool)*n);
    sc_comp_cnt = 0;
    stk = stack<int>();
    for(int i=0,step=0;i<n;i++) if(visit[i]==-1)
        visit[i] = step++, scc_dfs(i,step);
}

/**
 * Gabow's algorithm for finding a maximum match in a general (undirected) graph.
 *
 * Calculates a matching of the graphs in the array match[].
 * IMPORTANT: the provided graph must have three properties:
 * 1) The nodes are numbered from 1 to n. Node #0 is reserved for internal use.
 * 2) The provided graph must be an edge adjacency-list, which means that
 *      graph[v][i] must return the number of an edge. For knowing the nodes
 *      incident in an edge the array edge[][] is used.
 * 3) The edges are numbered from 0 to m-1.
 *
 * @param MAXN          The maximum number of nodes in the graph.
 * @param MAXM          The maximum number of edges in the graph.
 * @param graph         Edge adjecency-list description of the graph.
 * @param edge          Description of incident nodes in each edge. The edges
 *                      are numbered from 0 to m-1.
 * @param n             Number of nodes in the graph.
 * @param m             Number of edges in the graph.
 *
 * @return match        For every vertex i (between 1 and n), if i is a
 *                      matched vertex, then the edge (i,match[i]) is matched.
 *                      If i is not matched, then match[i] == 0.
 *
 * @time: O(|V|^3)
 * @test: (livearchive)4130, (timus)1099
 */

//........Params begin..................
#define MAXN 100
#define MAXM 100*100

vector< vector<int> > graph;
int edge[MAXM+1][2]; 
int n,m;
//........Params end..................

int match[MAXN+1]; 
int label[MAXN+1],first[MAXN+1];
queue<int> q;

void rematch(int v, int w){
    int t = match[v];
    match[v] = w;
    if(match[t] != v) return;
    if(label[v] <= n){
        match[t] = label[v];
        return rematch(label[v],t);
    }
    int e = label[v]-1-n,
        x = edge[e][0], y = edge[e][1];
    rematch(x,y);
    rematch(y,x);
}

inline void edge_label(int e){
    int x = edge[e][0], y = edge[e][1],
        r = first[x], s = first[y],
        flag = n+1+e, join;
    if(r==s) return;
    label[r] = label[s] = -flag;
    while(true){
        if(s!=0){int aux = s; s = r, r = aux;}
        r = first[label[match[r]]];
        if(label[r]==-flag){
            join = r;
            break;
        }
        label[r] = -flag;
    }
    for(int i=0;i<2;i++){
        int v = first[edge[e][i]];
        while(v!=join){
            label[v] = flag, first[v] = join;
            q.push(v);
            v = first[label[match[v]]];
        }
    }
    for(int i=1;i<=n;i++)
        if(label[i]>=0 && label[first[i]] >= 0)
            first[i] = join;
}

inline bool augpath(int u){
    memset(label,-1,sizeof(int)*(n+1));
    label[u] = first[u] = 0;
    q = queue<int>();
    q.push(u);
    while(!q.empty()){
        int x = q.front(); q.pop();
        for(int i=0;i<graph[x].size();i++){
            int e = graph[x][i],
                y = (edge[e][0] != x ? edge[e][0] : edge[e][1]);
            if(match[y] == 0 && y != u) {
                match[y] = x;
                rematch(x,y);
                return true;
            }
            if(label[y]>=0){
                edge_label(e);
                continue;
            }
            int v = match[y];
            if(label[v]<0){
                label[v] = x, first[v] = y;
                q.push(v);
            }
        }
    }
    return false;
}

//Main function, returns the number of matched edges found
inline int gabow(){
    memset(match,0,sizeof(int)*(n+1));
    int tot = 0;
    for(int i=1;i<=n;i++) {
        if(match[i]!=0) continue;
        if(augpath(i)) tot++;
    }
    return tot;
}

